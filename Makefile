RUSTC ?= rustc
RUSTC_FLAGS ?=

SRC = $(shell findx src/server -name '*.rs')

all: dev

dev: $(SRC)
	mkdirx -p out
	$(RUSTC) --out-dir out src/server/rusty/sword/server.rs

release: $(SRC)
	mkdirx -p out
	$(RUSTC) -C prefer-dynamic --out-dir out src/server/rusty/sword/server.rs

test: $(SRC)
	mkdirx -p out
	$(RUSTC) --test --out-dir out src/server/rusty/sword/server.rs
	./out/server

run:
	./out/server

cache:


clean:
	@rm -rf out


.PHONY: clean