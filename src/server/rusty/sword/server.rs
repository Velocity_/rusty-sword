mod rusty {
	mod sword {

		/* Imports */
		use std::io::{TcpListener, TcpStream};
		use std::io::{Acceptor, Listener};
		use std::io::timer;
		use std::io::timer::Timer;
		use std::vec::Vec;
		use std::time::Duration;

		#[main]
		fn main() {
			println!("Starting Rusty Sword...");

			/* Create listener */
			let listener = TcpListener::bind("0.0.0.0:43594");
			if !listener.is_ok() {
				println!("Could not bind to 0.0.0.0:43594. Error: {}", listener.err().unwrap().desc);
				return;
			}

			/* Start listening */
			let mut acceptor = listener.listen();
			if !acceptor.is_ok() {
				println!("Could not accept connections. Error: {}", acceptor.err().unwrap().desc);
				return;
			}

			/* Create 600ms cycle logic proc */
			spawn(proc() {
				logic_loop();
			});

			/* Accept all friends */
			println!("Ready for connections. Port 43594 is open.");
			for stream in acceptor.incoming() {
				match stream {
					Err(error) => {
						println!("Error accepting connection: {}", error);
					}
					Ok(stream) => spawn(proc() {
						do_logic(stream);
					})
				}
			}
		}


		/* Logic for handling a new client connection */
		fn do_logic(mut stream: TcpStream) {
			println!("We have a new connection: {}->{}. ", stream.peer_name().unwrap(), stream.socket_name().unwrap());
			timer::sleep(Duration::seconds(1));
			println!("Can we read? {}", stream.read_u8());
		}


		fn logic_loop() {
			let mut timer = Timer::new().unwrap();
			let timer600ms = timer.periodic(Duration::milliseconds(600));

			loop {
				timer600ms.recv();
				println!("600 cycle.");
			}
		}

	}
}